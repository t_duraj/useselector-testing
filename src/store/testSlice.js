import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

export const fetchUsers = createAsyncThunk(
  'users/fetchUsers',
  async (userId, thunkAPI) => {
    const response = await fetch('https://jsonplaceholder.typicode.com/users');
    const json = await response.json();
    return json;
  }
)

const testSlice = createSlice({
  name: 'testSlice',
  initialState: {
    isLoading: false,
    data: [],
    counterValue: 1,
  },
  reducers: {
    increment: state => {
      state.counterValue = state.counterValue + 1;
    },
  },
  extraReducers: {
    [fetchUsers.pending]: (state) => {
      state.isLoading = true;
    },
    [fetchUsers.fulfilled]: (state, action) => {
      state.data = action.payload;
      state.isLoading = false;
    }
  }
});

export default testSlice;
