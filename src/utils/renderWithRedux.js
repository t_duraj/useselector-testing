import React from 'react';
import { configureStore } from '@reduxjs/toolkit'
import { render } from '@testing-library/react';
import testSlice from '../store/testSlice';
import { Provider } from 'react-redux';


export function renderWithRedux(ui, { initialState }) {
  const actions = [];

  const observerMiddleware = () => next => async action => {
    actions.push(action);
    return next(action);
  };

  const store = configureStore({
      reducer: {
        test: testSlice.reducer,
      },
      middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(observerMiddleware),
      preloadedState: initialState,
    });

    const utils = {
      dispatch(action) {
        return store.dispatch(action);
      },
      getDispatchedActions() {
        return actions;
      },
      getState() {
        return store.getState();
      },
    };
  return {
    ...render(<Provider store={store}>{ui}</Provider>),
    ...utils,
  };
}