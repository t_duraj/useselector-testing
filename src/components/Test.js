import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import testSlice, { fetchUsers } from '../store/testSlice';

const TestComponent = () => {
  const dispatch = useDispatch();
  const { isLoading, counterValue, data } = useSelector(state => state.test);

  return (
    <div>
      <h2>Counter value: {counterValue}</h2>
      <button onClick={() => dispatch(testSlice.actions.increment())}>Increment counter value</button>
      <button onClick={() => dispatch(fetchUsers())}>Fetch data</button>
      {isLoading && 'Loading'}
      {data.map(({ id, name }) => (
        <h5 key={id}>{name}</h5>
      ))}
    </div>
  )
};

export default TestComponent;
