import React from 'react';
import { Provider } from 'react-redux';
import store from './store/store';
import './App.css';
import TestComponent from './components/Test';

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <header className="App-header">
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
        <TestComponent />
      </div>
    </Provider>
  );
}

export default App;
