import React from 'react';
import { fireEvent, waitFor } from '@testing-library/react';
import { renderWithRedux } from '../utils/renderWithRedux';
import TestComponent from '../components/Test';

const mockFetch = response => {
  jest.spyOn(global, "fetch").mockImplementation(() =>
    Promise.resolve({
      json: () => Promise.resolve(response)
    })
  );
}

test('Check increment', () => {
  const { getByText, getState } = renderWithRedux(<TestComponent />, {
    initialState: {
      test: { counterValue: 3, data: [] }
    }
  });

  expect(getState().test.counterValue).toBe(3);
  const button = getByText('Increment counter value');
  fireEvent.click(button);


  expect(getState().test.counterValue).toBe(4);
  const linkElement = getByText('Counter value: 4');
  expect(linkElement).toBeInTheDocument();
});

test('Loading status should be visible only when fetching', async () => {
  const data = [{ id: 1, name: 'Test user' }];
  mockFetch(data);

  const { getByText, getState, queryByText } = renderWithRedux(<TestComponent />, {
    initialState: {
      test: { counterValue: 3, data: [], isLoading: false }
    }
  });

  expect(getState().test.isLoading).toBe(false);
  expect(queryByText('Loading')).not.toBeInTheDocument();

  const button = getByText('Fetch data');
  fireEvent.click(button);
  expect(getState().test.isLoading).toBe(true);
  expect(getByText('Loading')).toBeInTheDocument();

  await waitFor(() => {
    expect(queryByText('Loading')).not.toBeInTheDocument();
    expect(getState().test.isLoading).toBe(false);
  });
});

test('Render fetched data', async () => {
  const data = [{ id: 1, name: 'Test user' }];
  mockFetch(data);

  const { getByText, getState, queryByText } = renderWithRedux(<TestComponent />, {
    initialState: {
      test: { counterValue: 3, data: [], isLoading: false }
    }
  });

  expect(getState().test.data).toStrictEqual([]);
  expect(queryByText(data[0].name)).not.toBeInTheDocument()

  const button = getByText('Fetch data');
  fireEvent.click(button);

  await waitFor(() => {
    expect(getByText(data[0].name)).toBeInTheDocument()
    expect(getState().test.data).toStrictEqual(data);
  })
});

test('Pending and fulfilled actions should be dispatched', async () => {
  const data = [{ id: 1, name: 'Test user' }];
  mockFetch(data);

  const { getByText, getDispatchedActions } = renderWithRedux(<TestComponent />, {
    initialState: {
      test: { counterValue: 3, data: [], isLoading: false }
    }
  });

  expect(getDispatchedActions()).toStrictEqual([]);

  const button = getByText('Fetch data');
  fireEvent.click(button);

  const pendingAction = 'users/fetchUsers/pending';
  const fulfilledAction = 'users/fetchUsers/fulfilled';

  await waitFor(() => {
    expect(getDispatchedActions().length).toBe(2);
    expect(getDispatchedActions().find(action => action.type === pendingAction)).toBeTruthy();
    expect(getDispatchedActions().find(action => action.type === fulfilledAction)).toBeTruthy();
  });
});

